# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 14:20:00 2018

Short scripts to test that Tensorflow and Keras work

Ho to install Tensorflow GPU version
  https://www.tensorflow.org/install/install_windows
  http://www.python36.com/install-tensorflow-gpu-windows/
  1) Install Microsoft Visual Studio Community 2015 Update 3
  2) Verify you have a CUDA-Capable GPU
       Win+R --> control /name Microsoft.DeviceManager
  3) Download and install the NVIDIA CUDA Toolkit 9.0
       https://developer.nvidia.com/cuda-toolkit-archive
  4) Reboot the system to load the NVIDIA drivers
  5) Check Cuda Toolkit is set to path OK
       Win+R: cmd -->  nvcc --version
  6) Download cuDNN 7.0.5, v 9.0. Copy content to corresponding 
     directories in Program Files \ NVIDIA GPU Computing Toolkit folder
        https://developer.nvidia.com/cudnn
  7) Install Tensorflow following https://www.tensorflow.org/install/install_windows
  8) Install keras

To choose between CPU and GPU
  https://stackoverflow.com/questions/40690598/can-keras-with-tensorflow-backend-be-forced-to-use-cpu-or-gpu-at-will

Using GPU
  https://www.tensorflow.org/programmers_guide/using_gpu
  "If a TensorFlow operation has both CPU and GPU implementations, the GPU devices will 
  be given priority when the operation is assigned to a device." 

@author: mikko hakala
"""

import tensorflow as tf
# from sklearn import svm


def get_available_gpus_cpus():
    from tensorflow.python.client import device_lib
    local_device_protos = device_lib.list_local_devices()
    gpus = [x.name for x in local_device_protos if x.device_type == 'GPU']
    cpus = [x.name for x in local_device_protos if x.device_type == 'CPU']
    print('gpus and cpus available:')
    return("gpus: " + str(gpus) + " cpus: " + str(cpus))



def tf_first_test():
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    print('Simple tensorflow matmul a*b')
    print(sess.run(c))



def test_cpu():
    with tf.device('/cpu:0'):
        a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
        b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
        c = tf.matmul(a, b)
        # Creates a session with log_device_placement set to True.
        sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    # Runs the op.
    print('Tensorflow matmul a*b')
    print("CPU:", sess.run(c))

def test_gpu():
    with tf.device('/gpu:0'):
        a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
        b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
        c = tf.matmul(a, b)
        # Creates a session with log_device_placement set to True.
        sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    # Runs the op.
    print('Tensorflow matmul a*b')
    print("GPU:", sess.run(c))




def keras_first_test(device):
    # https://machinelearningmastery.com/tutorial-first-neural-network-python-keras/

    from keras.models import Sequential
    from keras.layers import Dense
    import numpy
    # fix random seed for reproducibility
    numpy.random.seed(7)

    print('Keras quick test')
    print('Device:', device)


    # load pima indians dataset
    dataset = numpy.loadtxt("pima-indians-diabetes.csv", delimiter=",")
    # split into input (X) and output (Y) variables
    X = dataset[:,0:8]
    Y = dataset[:,8]

    with tf.device('/cpu:0'):
    #if(True):
        # create model
        model = Sequential()
        model.add(Dense(12, input_dim=8, activation='relu'))
        model.add(Dense(8, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        # Compile model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        # Fit the model
        model.fit(X, Y, epochs=2, batch_size=10)  # was: 150
        
    # evaluate the model
    scores = model.evaluate(X, Y)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))



def main(): 

    print(get_available_gpus_cpus())
    input("press enter")

    tf_first_test()
    input("press enter")

    test_cpu()
    input("press enter")

    test_gpu()
    input("press enter")


    #device = '/cpu:0'
    device = '/gpu:0'
    keras_first_test(device)
    
    

    print('Done')


if __name__ == '__main__': 
     main() 


